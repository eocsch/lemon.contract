/**
 *  @file
 *  @copyright defined in eos/LICENSE.txt
 */
#pragma once

#include <lemon.system/native.hpp>
#include <lemonlib/asset.hpp>
#include <lemonlib/time.hpp>
#include <lemonlib/privileged.hpp>
#include <lemonlib/singleton.hpp>
//#include <eosio.system/exchange_state.hpp>

#include <string>
#include <deque>
#include <type_traits>
#include <optional>

#ifdef CHANNEL_RAM_AND_NAMEBID_FEES_TO_REX
#undef CHANNEL_RAM_AND_NAMEBID_FEES_TO_REX
#endif

#define CHANNEL_RAM_AND_NAMEBID_FEES_TO_REX 1

namespace lemonsystem {

   using lemon::name;
   using lemon::asset;
   using lemon::symbol;
   using lemon::symbol_code;
   using lemon::indexed_by;
   using lemon::const_mem_fun;
   using lemon::block_timestamp;
   using lemon::time_point;
   using lemon::time_point_sec;
   using lemon::microseconds;
   using lemon::datastream;
   using lemon::check;

   template<typename E, typename F>
   static inline auto has_field( F flags, E field )
   -> std::enable_if_t< std::is_integral_v<F> && std::is_unsigned_v<F> &&
                        std::is_enum_v<E> && std::is_same_v< F, std::underlying_type_t<E> >, bool>
   {
      return ( (flags & static_cast<F>(field)) != 0 );
   }

   template<typename E, typename F>
   static inline auto set_field( F flags, E field, bool value = true )
   -> std::enable_if_t< std::is_integral_v<F> && std::is_unsigned_v<F> &&
                        std::is_enum_v<E> && std::is_same_v< F, std::underlying_type_t<E> >, F >
   {
      if( value )
         return ( flags | static_cast<F>(field) );
      else
         return ( flags & ~static_cast<F>(field) );
   }

   struct [[lemon::table, lemon::contract("lemon.system")]] name_bid {
     name            newname;
     name            high_bidder;
     int64_t         high_bid = 0; ///< negative high_bid == closed auction waiting to be claimed
     time_point      last_bid_time;

     uint64_t primary_key()const { return newname.value;                    }
     uint64_t by_high_bid()const { return static_cast<uint64_t>(-high_bid); }
   };

   struct [[lemon::table, lemon::contract("lemon.system")]] bid_refund {
      name         bidder;
      asset        amount;

      uint64_t primary_key()const { return bidder.value; }
   };

   typedef lemon::multi_index< "namebids"_n, name_bid,
                               indexed_by<"highbid"_n, const_mem_fun<name_bid, uint64_t, &name_bid::by_high_bid>  >
                             > name_bid_table;

   typedef lemon::multi_index< "bidrefunds"_n, bid_refund > bid_refund_table;

   struct [[lemon::table("global"), lemon::contract("lemon.system")]] eosio_global_state : lemon::blockchain_parameters {
      uint64_t free_ram()const { return max_ram_size - total_ram_bytes_reserved; }

      uint64_t             max_ram_size = 64ll*1024 * 1024 * 1024;
      uint64_t             total_ram_bytes_reserved = 0;
      int64_t              total_ram_stake = 0;

      block_timestamp      last_producer_schedule_update;
      time_point           last_pervote_bucket_fill;
      int64_t              pervote_bucket = 0;
      int64_t              perblock_bucket = 0;
      uint32_t             total_unpaid_blocks = 0; /// all blocks which have been produced but not paid
      int64_t              total_activated_stake = 0;
      time_point           thresh_activated_stake_time;
      uint16_t             last_producer_schedule_size = 0;
      double               total_producer_vote_weight = 0; /// the sum of all producer votes
      block_timestamp      last_name_close;

      uint8_t              max_producer_schedule_size = 21;
      int64_t              min_pervote_daily_pay      = 100'0000;
      int64_t              min_activated_stake        = 150'000'000'0000;
      int64_t              useconds_per_day           = 24 * 3600 * int64_t(1000000); // redefine meaning of `day` to regulate something
      double              continuous_rate            = 0.04879; // 5% annual rate
      double              to_producers_rate          = 0.2;
      double              to_bpay_rate               = 0.25; // producer block pay rate with regard to producers pay
      double              to_voter_bonus_rate        = 0; // voter bonus rate with regard to producer voting pay
      uint32_t             refund_delay_sec           = 3*24*3600;
      int64_t              ram_gift_bytes             = 1400;

      // explicit serialization macro is not necessary, used here only to improve compilation time
      EOSLIB_SERIALIZE_DERIVED( eosio_global_state, lemon::blockchain_parameters,
                                (max_ram_size)(total_ram_bytes_reserved)(total_ram_stake)
                                (last_producer_schedule_update)(last_pervote_bucket_fill)
                                (pervote_bucket)(perblock_bucket)(total_unpaid_blocks)(total_activated_stake)(thresh_activated_stake_time)
                                (last_producer_schedule_size)(total_producer_vote_weight)(last_name_close)
                                (max_producer_schedule_size)(min_pervote_daily_pay)(min_activated_stake)
                                (useconds_per_day)(continuous_rate)
                                (to_producers_rate)(to_bpay_rate)(to_voter_bonus_rate)
                                (refund_delay_sec)(ram_gift_bytes) )
   };

   /**
    * Defines new global state parameters added after version 1.0
    */
   struct [[lemon::table("global2"), lemon::contract("lemon.system")]] eosio_global_state2 {
      eosio_global_state2(){}

      uint16_t          new_ram_per_block = 0;
      block_timestamp   last_ram_increase;
      block_timestamp   last_block_num; /* deprecated */
      double            total_producer_votepay_share = 0;
      uint8_t           revision = 0; ///< used to track version updates in the future.

      EOSLIB_SERIALIZE( eosio_global_state2, (new_ram_per_block)(last_ram_increase)(last_block_num)
                        (total_producer_votepay_share)(revision) )
   };

   struct [[lemon::table("global3"), lemon::contract("lemon.system")]] eosio_global_state3 {
      eosio_global_state3() { }
      time_point        last_vpay_state_update;
      double            total_vpay_share_change_rate = 0;

      EOSLIB_SERIALIZE( eosio_global_state3, (last_vpay_state_update)(total_vpay_share_change_rate) )
   };

   struct [[lemon::table, lemon::contract("lemon.system")]] producer_info {
      name                  owner;
      double                total_votes = 0;
      lemon::public_key     producer_key; /// a packed public key object
      bool                  is_active = true;
      std::string           url;
      uint32_t              unpaid_blocks = 0;
      time_point            last_claim_time;
      uint16_t              location = 0;

      uint64_t primary_key()const { return owner.value;                             }
      double   by_votes()const    { return is_active ? -total_votes : total_votes;  }
      bool     active()const      { return is_active;                               }
      void     deactivate()       { producer_key = public_key(); is_active = false; }

      // explicit serialization macro is not necessary, used here only to improve compilation time
      EOSLIB_SERIALIZE( producer_info, (owner)(total_votes)(producer_key)(is_active)(url)
                        (unpaid_blocks)(last_claim_time)(location) )
   };

   struct [[lemon::table, lemon::contract("lemon.system")]] producer_info2 {
      name            owner;
      double          votepay_share = 0;
      time_point      last_votepay_share_update;

      uint64_t primary_key()const { return owner.value; }

      // explicit serialization macro is not necessary, used here only to improve compilation time
      EOSLIB_SERIALIZE( producer_info2, (owner)(votepay_share)(last_votepay_share_update) )
   };

   struct [[lemon::table, lemon::contract("lemon.system")]] voter_info {
      name                owner;     /// the voter
      name                proxy;     /// the proxy set by the voter, if any
      std::vector<name>   producers; /// the producers approved by this voter if no proxy set
      int64_t             staked = 0;

     
      double              last_vote_weight = 0; /// the vote weight cast the last time the vote was updated

     
      double              proxied_vote_weight= 0; /// the total vote weight delegated to this voter as a proxy
      bool                is_proxy = 0; /// whether the voter is a proxy for others

      time_point          last_change_time;

      uint32_t            flags1 = 0;
      uint32_t            reserved2 = 0;
      lemon::asset        reserved3;

      uint64_t primary_key()const { return owner.value; }

      enum class flags1_fields : uint32_t {
         ram_managed = 1,
         net_managed = 2,
         cpu_managed = 4
      };

      // explicit serialization macro is not necessary, used here only to improve compilation time
      EOSLIB_SERIALIZE( voter_info, (owner)(proxy)(producers)(staked)(last_vote_weight)(proxied_vote_weight)(is_proxy)(last_change_time)(flags1)(reserved2)(reserved3) )
   };

   struct [[lemon::table, lemon::contract("lemon.system")]] voter_bonus {
       name producer;
       lemon::asset balance;

       uint64_t primary_key()const { return producer.value; }

       EOSLIB_SERIALIZE( voter_bonus, (producer)(balance))
   };

   typedef lemon::multi_index< "voters"_n, voter_info >  voters_table;

   typedef lemon::multi_index< "voterbonus"_n, voter_bonus > voter_bonus_table;

   typedef lemon::multi_index< "producers"_n, producer_info,
                               indexed_by<"prototalvote"_n, const_mem_fun<producer_info, double, &producer_info::by_votes>  >
                             > producers_table;
   typedef lemon::multi_index< "producers2"_n, producer_info2 > producers_table2;

   typedef lemon::singleton< "global"_n, eosio_global_state >   global_state_singleton;
   typedef lemon::singleton< "global2"_n, eosio_global_state2 > global_state2_singleton;
   typedef lemon::singleton< "global3"_n, eosio_global_state3 > global_state3_singleton;

   static constexpr uint32_t     seconds_per_day = 24 * 3600;

   class [[lemon::contract("lemon.system")]] system_contract : public native {

      private:
         voters_table            _voters;
         voter_bonus_table       _voterbonus;
         producers_table         _producers;
         producers_table2        _producers2;
         global_state_singleton  _global;
         global_state2_singleton _global2;
         global_state3_singleton _global3;
         eosio_global_state      _gstate;
         eosio_global_state2     _gstate2;
         eosio_global_state3     _gstate3;
       

      public:
         static constexpr lemon::name active_permission{"active"_n};
         static constexpr lemon::name token_account{"lemon.token"_n};
         static constexpr lemon::name ram_account{"lemon.ram"_n};
         static constexpr lemon::name ramfee_account{"lemon.ramfee"_n};
         static constexpr lemon::name stake_account{"lemon.stake"_n};
         static constexpr lemon::name bpay_account{"lemon.bpay"_n};
         static constexpr lemon::name vpay_account{"lemon.vpay"_n};
         static constexpr lemon::name names_account{"lemon.names"_n};
         static constexpr lemon::name saving_account{"lemon.saving"_n};
         static constexpr lemon::name rex_account{"lemon.rex"_n};
         static constexpr lemon::name null_account{"lemon.null"_n};
         static constexpr symbol ramcore_symbol = symbol(symbol_code("RAMCORE"), 4);
         static constexpr symbol ram_symbol     = symbol(symbol_code("RAM"), 0);
         static constexpr symbol rex_symbol     = symbol(symbol_code("REX"), 4);

         system_contract( name s, name code, datastream<const char*> ds );
         ~system_contract();
	
         [[lemon::action]]
         void onblock( ignore<block_header> header );
       
         [[lemon::action]]
         void regproducer( const name producer, const public_key& producer_key, const std::string& url, uint16_t location , const name regaccount);

         // [[lemon::action]]
         // void unregprod( const name producer );

         [[lemon::action]]
         void unregprod( const name producer, const name unregaccount );

         

         [[lemon::action]]
         void voteproducer( const name voter, const name proxy, const std::vector<name>& producers );

         [[lemon::action]]
         void setparams( const lemon::blockchain_parameters& params );

         // functions defined in producer_pay.cpp
        

         [[lemon::action]]
         void setpriv( name account, uint8_t is_priv );

         [[lemon::action]]
         void rmvproducer( name producer );

         [[lemon::action]]
         void updtrevision( uint8_t revision );

        

         [[lemon::action]]
         void setglobal( std::string name, std::string value );

         [[lemon::action]]
         void updtbwlist(uint8_t type, const std::vector<std::string>& add, const std::vector<std::string>& rmv);
        
         using regproducer_action = lemon::action_wrapper<"regproducer"_n, &system_contract::regproducer>;
         using unregprod_action = lemon::action_wrapper<"unregprod"_n, &system_contract::unregprod>;
         using voteproducer_action = lemon::action_wrapper<"voteproducer"_n, &system_contract::voteproducer>;
        
         using rmvproducer_action = lemon::action_wrapper<"rmvproducer"_n, &system_contract::rmvproducer>;
         using updtrevision_action = lemon::action_wrapper<"updtrevision"_n, &system_contract::updtrevision>;
         using setpriv_action = lemon::action_wrapper<"setpriv"_n, &system_contract::setpriv>;
       

      private:

         // Implementation details:
	
         //defined in eosio.system.cpp
         static eosio_global_state get_default_parameters();
         static time_point current_time_point();
         static time_point_sec current_time_point_sec();
         static block_timestamp current_block_time();
         symbol core_symbol()const;
         
     
         // defined in voting.hpp
         void update_elected_producers( block_timestamp timestamp );
         void update_votes( const name voter, const name proxy, const std::vector<name>& producers, bool voting );
         
         template <auto system_contract::*...Ptrs>
         class registration {
            public:
               template <auto system_contract::*P, auto system_contract::*...Ps>
               struct for_each {
                  template <typename... Args>
                  static constexpr void call( system_contract* this_contract, Args&&... args )
                  {
                     std::invoke( P, this_contract, std::forward<Args>(args)... );
                     for_each<Ps...>::call( this_contract, std::forward<Args>(args)... );
                  }
               };
               template <auto system_contract::*P>
               struct for_each<P> {
                  template <typename... Args>
                  static constexpr void call( system_contract* this_contract, Args&&... args )
                  {
                     std::invoke( P, this_contract, std::forward<Args>(args)... );
                  }
               };

               template <typename... Args>
               constexpr void operator() ( Args&&... args )
               {
                  for_each<Ptrs...>::call( this_contract, std::forward<Args>(args)... );
               }

               system_contract* this_contract;
         };

        
   };

} /// lemonsystem
