/**
 *  @file
 *  @copyright defined in eos/LICENSE.txt
 */
#pragma once

#include <lemonlib/action.hpp>
#include <lemonlib/public_key.hpp>
#include <lemonlib/print.hpp>
#include <lemonlib/privileged.h>
#include <lemonlib/producer_schedule.hpp>
#include <lemonlib/contract.hpp>
#include <lemonlib/ignore.hpp>

namespace lemonsystem {
   using lemon::name;
   using lemon::permission_level;
   using lemon::public_key;
   using lemon::ignore;

   struct permission_level_weight {
      permission_level  permission;
      uint16_t          weight;

      // explicit serialization macro is not necessary, used here only to improve compilation time
      EOSLIB_SERIALIZE( permission_level_weight, (permission)(weight) )
   };

   struct key_weight {
      lemon::public_key  key;
      uint16_t           weight;

      // explicit serialization macro is not necessary, used here only to improve compilation time
      EOSLIB_SERIALIZE( key_weight, (key)(weight) )
   };

   struct wait_weight {
      uint32_t           wait_sec;
      uint16_t           weight;

      // explicit serialization macro is not necessary, used here only to improve compilation time
      EOSLIB_SERIALIZE( wait_weight, (wait_sec)(weight) )
   };

   struct authority {
      uint32_t                              threshold = 0;
      std::vector<key_weight>               keys;
      std::vector<permission_level_weight>  accounts;
      std::vector<wait_weight>              waits;

      // explicit serialization macro is not necessary, used here only to improve compilation time
      EOSLIB_SERIALIZE( authority, (threshold)(keys)(accounts)(waits) )
   };

   struct block_header {
      uint32_t                                  timestamp;
      name                                      producer;
      uint16_t                                  confirmed = 0;
      capi_checksum256                          previous;
      capi_checksum256                          transaction_mroot;
      capi_checksum256                          action_mroot;
      uint32_t                                  schedule_version = 0;
      std::optional<lemon::producer_schedule>   new_producers;

      // explicit serialization macro is not necessary, used here only to improve compilation time
      EOSLIB_SERIALIZE(block_header, (timestamp)(producer)(confirmed)(previous)(transaction_mroot)(action_mroot)
                                     (schedule_version)(new_producers))
   };


   struct [[lemon::table("abihash"), lemon::contract("lemon.system")]] abi_hash {
      name              owner;
      capi_checksum256  hash;
      uint64_t primary_key()const { return owner.value; }

      EOSLIB_SERIALIZE( abi_hash, (owner)(hash) )
   };

   /*
    * Method parameters commented out to prevent generation of code that parses input data.
    */
   class [[lemon::contract("lemon.system")]] native : public lemon::contract {
      public:

         using lemon::contract::contract;

         /**
          *  Called after a new account is created. This code enforces resource-limits rules
          *  for new accounts as well as new account naming conventions.
          *
          *  1. accounts cannot contain '.' symbols which forces all acccounts to be 12
          *  characters long without '.' until a future account auction process is implemented
          *  which prevents name squatting.
          *
          *  2. new accounts must stake a minimal number of tokens (as set in system parameters)
          *     therefore, this method will execute an inline buyram from receiver for newacnt in
          *     an amount equal to the current new account creation fee.
          */
      

         [[lemon::action]]
         void updateauth(  ignore<name>  account,
                           ignore<name>  permission,
                           ignore<name>  parent,
                           ignore<authority> auth ) {}

         [[lemon::action]]
         void deleteauth( ignore<name>  account,
                          ignore<name>  permission ) {}

         [[lemon::action]]
         void linkauth(  ignore<name>    account,
                         ignore<name>    code,
                         ignore<name>    type,
                         ignore<name>    requirement  ) {}

         [[lemon::action]]
         void unlinkauth( ignore<name>  account,
                          ignore<name>  code,
                          ignore<name>  type ) {}

         [[lemon::action]]
         void canceldelay( ignore<permission_level> canceling_auth, ignore<capi_checksum256> trx_id ) {}

         [[lemon::action]]
         void onerror( ignore<uint128_t> sender_id, ignore<std::vector<char>> sent_trx ) {}

         [[lemon::action]]
         void setabi( name account, const std::vector<char>& abi );

         [[lemon::action]]
         void setcode( name account, uint8_t vmtype, uint8_t vmversion, const std::vector<char>& code ) {}

         using updateauth_action = lemon::action_wrapper<"updateauth"_n, &native::updateauth>;
         using deleteauth_action = lemon::action_wrapper<"deleteauth"_n, &native::deleteauth>;
         using linkauth_action = lemon::action_wrapper<"linkauth"_n, &native::linkauth>;
         using unlinkauth_action = lemon::action_wrapper<"unlinkauth"_n, &native::unlinkauth>;
         using canceldelay_action = lemon::action_wrapper<"canceldelay"_n, &native::canceldelay>;
         using setcode_action = lemon::action_wrapper<"setcode"_n, &native::setcode>;
         using setabi_action = lemon::action_wrapper<"setabi"_n, &native::setabi>;
   };
}
