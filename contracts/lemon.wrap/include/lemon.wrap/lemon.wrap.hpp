#pragma once

#include <lemonlib/lemonc.hpp>
#include <lemonlib/ignore.hpp>
#include <lemonlib/transaction.hpp>

namespace lemon {

   class [[lemon::contract("lemon.wrap")]] wrap : public contract {
      public:
         using contract::contract;

         [[lemon::action]]
         void exec( ignore<name> executer, ignore<transaction> trx );

         using exec_action = lemon::action_wrapper<"exec"_n, &wrap::exec>;
   };

} /// namespace eosio
