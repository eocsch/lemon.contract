add_contract(lemon.wrap lemon.wrap ${CMAKE_CURRENT_SOURCE_DIR}/src/lemon.wrap.cpp)

target_include_directories(lemon.wrap
   PUBLIC
   ${CMAKE_CURRENT_SOURCE_DIR}/include)

set_target_properties(lemon.wrap
   PROPERTIES
   RUNTIME_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}")
